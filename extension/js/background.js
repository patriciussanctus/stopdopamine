/*The MIT License
 *
 * Copyright (C) 2016,2017 Dmytro Kalchenko <dmytro.v.kalcheno@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */


var helper =
{
	extractDomain: function(url)
	{
		var parser = document.createElement('a');
		parser.href = url;

		return parser.hostname;
	},

	extractElement: function(list, predicate)
	{
		for (var i = 0; i < list.length; i++)
		{
			if(predicate(list[i]))
				return list[i];
		}

		return null;
	}
};

function banDomain()
{
	chrome.tabs.query({active: true, currentWindow: true}, function(tabs)
	{
		var domain = helper.extractDomain(tabs[0].url);

		chrome.storage.local.get({ bannedDomains: [] }, function(result)
		{
			var bannedDomains = result.bannedDomains;
			var banned = helper.extractElement(bannedDomains, x => x === domain);

			if(banned === null)
			{
				if(domain.includes("www."))
					bannedDomains.push(domain.split("www.")[1]);
				else
					bannedDomains.push("www." + domain);

				bannedDomains.push(domain);
			}

			chrome.storage.local.set({ bannedDomains: bannedDomains }, function()
			{
				if(chrome.runtime.lastError)
				{
					alert("Error");
					console.log("Error: could not ban domain.");
					console.log(chrome.runtime.lastError);
				}

				chrome.tabs.reload(tabs[0].id);
			});
		});
	});
}

chrome.runtime.onInstalled.addListener(function(details)
{
	if(details.reason == "install")
	{
		chrome.contextMenus.create({"id":"distractoff-context-add", "title": "Ban domain","contexts": ["page"]});
		chrome.contextMenus.onClicked.addListener(banDomain);
	}
	else if(details.reason == "update")
	{
		chrome.tabs.create({url: chrome.extension.getURL("view/changelog.html")});
	}
});

chrome.tabs.onRemoved.addListener(function(id, info)
{
	chrome.tabs.query({}, function(tabs)
	{
		chrome.storage.local.get({ tempAllowed: [] }, function(result)
		{
			var tempAllowed = result.tempAllowed;

			for(var i = 0; i < tempAllowed.length; i++)
			{
				var x = helper.extractElement(tabs, x => helper.extractDomain(x.url) === tempAllowed[i])
				if(x === null)
				{
					tempAllowed.splice(i, 1);
					i--;
				}
			}

			chrome.storage.local.set({ tempAllowed: tempAllowed }, function()
			{
				if(chrome.runtime.lastError)
				{
					console.log("Error: could not ban domain.");
					console.log(chrome.runtime.lastError);
				}
			});
		});
	});
});

chrome.runtime.onMessage.addListener(function(request, sender)
{
	if(request.type === "close")
	{
		chrome.tabs.query({ active: true, currentWindow: true }, function(tabs)
		{
			chrome.tabs.remove(tabs[0].id);
		});
	}
	else if(request.type === "settings")
	{
		chrome.tabs.create({ url: "../view/settings.html" });
	}
	else if(request.type === "donate")
	{
		chrome.tabs.create({url: "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ZBMDV6Q7N53MW"});
	}
});
