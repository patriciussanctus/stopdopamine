/*The MIT License
 *
 * Copyright (C) 2016,2017 Dmytro Kalchenko <dmytro.v.kalcheno@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */


var helper =
{
	extractDomain: function(url)
	{
		var parser = document.createElement('a');
		parser.href = url;

		return parser.hostname;
	},

	extractElement: function(list, predicate)
	{
		for (var i = 0; i < list.length; i++)
		{
			if(predicate(list[i]))
				return list[i];
		}

		return null;
	}
};

function banDomain()
{
	chrome.tabs.query({active: true, currentWindow: true}, function(tabs)
	{
		var domain = helper.extractDomain(tabs[0].url);

		chrome.storage.local.get({ bannedDomains: [] }, function(result)
		{
			var bannedDomains = result.bannedDomains;
			var banned = helper.extractElement(bannedDomains, x => x === domain);

			if(banned === null)
			{
				if(domain.includes("www."))
					bannedDomains.push(domain.split("www.")[1]);
				else
					bannedDomains.push("www." + domain);

				bannedDomains.push(domain);
			}

			chrome.storage.local.set({ bannedDomains: bannedDomains }, function()
			{
				if(chrome.runtime.lastError)
				{
					console.log("Error: could not ban domain.");
					console.log(chrome.runtime.lastError);
				}

				chrome.tabs.reload(tabs[0].id);
			});
		});
	});
}

function setStatusUI(uiText, uiBtn, status)
{
	uiText.textContent = status;
	uiText.className = status.toLowerCase();

	if(status === "Running")
		uiBtn.textContent = "Stop";
	else
		uiBtn.textContent = "Start";
}

document.addEventListener('DOMContentLoaded', function()
{
	var btnAdd = document.getElementById('btn-add');
	var btnSettings = document.getElementById('btn-settings');
	//var btnDonate = document.getElementById('btn-donate');
	var btnToggleStatus =  document.getElementById('btn-toggle-status');
	var textStatus =  document.getElementById('text-status');

	btnAdd.addEventListener('click', banDomain);
	btnSettings.addEventListener('click', function() { chrome.tabs.create({ url: "view/settings.html" }); });
	//btnDonate.addEventListener("click", function() { chrome.runtime.sendMessage({type:"donate"}); });

	chrome.storage.local.get({status:"Running"}, function(result)
	{
		status = result.status;

		setStatusUI(textStatus, btnToggleStatus, status);
	});

	btnToggleStatus.addEventListener("click", function()
	{
		chrome.storage.local.get({status:"Running"}, function(result)
		{
			status = result.status;

			if(status === "Running")
			{
				chrome.storage.local.set({ status:"Stopped" }, function()
				{
					if(chrome.runtime.lastError)
					{
						console.log("Error: could not stop the service.");
						console.log(chrome.runtime.lastError);
					}

					setStatusUI(textStatus, btnToggleStatus, "Stopped");
				});
			}
			else
			{
				chrome.storage.local.set({ status:"Running" }, function()
				{
					if(chrome.runtime.lastError)
					{
						console.log("Error: could not start the service.");
						console.log(chrome.runtime.lastError);
					}

					setStatusUI(textStatus, btnToggleStatus, "Running");
				});
			}
		});
	});
});
